=== Download PDF After Submit Form ===
Contributors: shahinurislam
Donate link: https://forms.gle/EAtaCDDDxhcU5fva7
Tags: Download pdf, form, restricted form
Requires at least: 5.4
Tested up to: 6.1
Stable tag: 2.0
Requires PHP: 7.4
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Just another Download PDF after Submit Form plugins. Simple but flexible.

== Description ==

Download any pdf after submit form shortcode for WordPress. Take full control over your WordPress site, build any shortcode paste you can imagine – no programming knowledge required.

= Docs & Support =

You can find [docs](https://gitlab.com/shahinurislam/download-pdf-after-submit-form), [FAQ](https://gitlab.com/shahinurislam/download-pdf-after-submit-form) and more detailed information about Download PDF after Submit Form on [gitlab](https://gitlab.com/shahinurislam/download-pdf-after-submit-form). If you were unable to find the answer to your question on the FAQ or in any of the documentation, you should check the [support forum](https://wordpress.org/support/plugin/download-pdf-after-submit-form) on WordPress.org. If you can't locate any topics that pertain to your particular issue, post a new topic for it.

== Why Use Download PDF After Submit Form? ==
Download PDF After Submit Form gives you all the features needed to create a files restricted permission without any hassle.

https://youtu.be/Q6J6LMn5D18

= Download PDF after Submit Form Needs Your Support =

It is hard to continue development and support for this free plugin without contributions from users like you. If you enjoy using Download PDF after Submit Form and find it useful, please consider [__making a donation__](https://forms.gle/EAtaCDDDxhcU5fva7). Your donation will help encourage and support the plugin's continued development and better user support. Find on gitlab. [Gitlab](https://gitlab.com/shahinurislam/download-pdf-after-submit-form) 

== Installation ==

1. Upload the entire `Download PDF after Submit Form` folder to the `/wp-content/plugins/` directory.
1. Activate the plugin through the 'Plugins' menu in WordPress.

You will find 'Contact' menu in your WordPress admin panel.

For basic usage, you can also have a look at the [plugin web site](https://gitlab.com/shahinurislam/download-pdf-after-submit-form).

== Frequently Asked Questions ==

Do you have questions or issues with Download PDF after Submit Form? Use these support channels appropriately.

= Download PDF after Submit Form is responsive? =

Yes this plugin are fully responsive and support every mobile device.

= What about this plugin? =

This plugin is Download PDF after Submit Form.

== Upgrade Notice ==

This is new version 2.0

== Screenshots ==

1. screenshot-1.png 
2. screenshot-2.png 
3. screenshot-3.png 

== Changelog ==

= 2.0 =

* Change post type on single file.
* Multiple design.

= 1.0 =

* Add new post type.
* Generate shortcode place and show the main file.
