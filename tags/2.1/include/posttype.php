<?php
//<-- custom post --> 
add_action( 'init', 'dpbsf_download_posttype' );
function dpbsf_download_posttype() {
 $labels = array(
  'name'               => _x( 'Infomats', 'post type general name', 'dpbsf' ),
  'singular_name'      => _x( 'Infomat', 'post type singular name', 'dpbsf' ),
  'menu_name'          => _x( 'Infomats', 'admin menu', 'dpbsf' ),
  'name_admin_bar'     => _x( 'Infomat', 'add new on admin bar', 'dpbsf' ),
  'add_new'            => _x( 'Add New', 'Infomat', 'dpbsf' ),
  'add_new_item'       => __( 'Add New Infomat', 'dpbsf' ),
  'new_item'           => __( 'New Infomat', 'dpbsf' ),
  'edit_item'          => __( 'Edit Infomat', 'dpbsf' ),
  'view_item'          => __( 'View Infomat', 'dpbsf' ),
  'all_items'          => __( 'All Infomats', 'dpbsf' ),
  'search_items'       => __( 'Search Infomats', 'dpbsf' ),
  'parent_item_colon'  => __( 'Parent Infomats:', 'dpbsf' ),
  'not_found'          => __( 'No Infomats found.', 'dpbsf' ),
  'not_found_in_trash' => __( 'No Infomats found in Trash.', 'dpbsf' )
 );
 $args = array(
  'labels'             => $labels,
  'description'        => __( 'Description.', 'dpbsf' ),
  'public'             => true,
  'publicly_queryable' => true,
  'show_ui'            => true,
  'show_in_menu'       => true,
  'query_var'          => true,
  'rewrite'            => array( 'slug' => 'infomat' ),
  'capability_type'    => 'post',
  'has_archive'        => true,
  'hierarchical'       => false,
  'menu_position'      => null,
  'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
 );
 register_post_type( 'infomat', $args );
} 
?>