<?php
/*
Plugin Name: Download PDF After Submit Form
Plugin URI: https://gitlab.com/shahinurislam/download-pdf-after-submit-form
Description: Download any pdf After submit form.
Author: Md. Shahinur Islam
Version: 1.0
Author URI: https://profiles.wordpress.org/shahinurislam/
*/
global $session;
session_start(); 
//--------------------- Create css and js ---------------------------//
define( 'DPBSF_PLUGIN', __FILE__ );
define( 'DPBSF_PLUGIN_DIR', untrailingslashit( dirname( DPBSF_PLUGIN ) ) );
require_once DPBSF_PLUGIN_DIR . '/include/enqueue.php';
//-------------All post show------------//
function dpbsf_shortcode_wrapper($atts) {
ob_start(); 
//set attributies
$atts = shortcode_atts(
	array(
		'urlname' => '',
		'title' => ''
	), $atts, 'helloshahin'); 
 //check all input fileds
 if(isset($_POST['submitted'])) {
	if(trim($_POST['contactName']) === '') {
		$nameError = 'Please enter your name.';
		$hasError = true;
	} else {
		$name = sanitize_text_field(trim($_POST['contactName']));
	}
	if(trim($_POST['email']) === '')  {
		$emailError = 'Please enter your email address.';
		$hasError = true;
	} else if (!preg_match("/^[[:alnum:]][a-z0-9_.-]*@[a-z0-9.-]+\.[a-z]{2,4}$/i", trim($_POST['email']))) {
		$emailError = 'You entered an invalid email address.';
		$hasError = true;
	} else {
		$email = sanitize_email(trim($_POST['email']));
	}
	if(trim($_POST['comments']) === '') {
		$commentError = 'Please enter a message.';
		$hasError = true;
	} else {
		if(function_exists('stripslashes')) {
			$comments = sanitize_text_field(stripslashes(trim($_POST['comments'])));
		} else {
			$comments = sanitize_text_field(trim($_POST['comments']));
		}
	}
	if(!isset($hasError)) {
		$emailTo = get_option('tz_email');
		if (!isset($emailTo) || ($emailTo == '') ){
			$emailTo = get_option('admin_email');
		}
		$donloadurl = sanitize_url(trim($_POST['urlnameinput'])); //get input url 
		$subject = '[PHP Snippets] From '.$name;
		$body = "Name: $name \n\nEmail: $email \n\nComments: $comments \nurl: $donloadurl";
		$headers = 'From: '.$name.' <'.$emailTo.'>' . "\r\n" . 'Reply-To: ' . $email;
		//wp_mail($emailTo, $subject, $body, $headers);
		$emailSent = true; 
		$_SESSION['arrayImg'] = "shahin"; //start session
	}
} ?> 
 <!-- Trigger/Open The Modal <button id="myBtn">Download</button>-->
 <?php 
 //session validation
 if($_SESSION['arrayImg'] == "shahin"){  ?>
	<style>
	/*display none when session true*/
		.download{display:none}
		#contactForm{display:none}
	</style>
	<?php
		$urlname = sanitize_url($atts['urlname']); 
	?>
<!-- new btn by jquery -->
<button class="dwn-now" id="downloadButton<?php esc_html_e($atts['title']);?>">Download</button>
<!--click to download-->
   <script>  
   jQuery('#downloadButton<?php esc_html_e($atts['title']);?>').click(function () {
	  axios({
		  url:'<?php esc_html_e($urlname); ?>',
		  method:'GET',
		  responseType: 'blob'
      })
      .then((response) => {
		 const url = window.URL
		 .createObjectURL(new Blob([response.data],{
			 "type": "text/pdf;charset=utf8;"
		 }));
		const link = document.createElement('a');
		link.href = url;
		link.setAttribute('download', '<?php esc_html_e($atts['title']);?>.pdf');
		document.body.appendChild(link);
		link.click();
      })
   });
   </script> 	
<?php }else{  ?>
	<!-- session validation failed to start modal-->
	<?php $echos = "myModal".esc_html($atts['title']);?>
	<button id="<?php  esc_html_e($echos);?>1" class="dwn-now">Download</button>
<?php	}  ?>
<!-- main content-->
<div class="entry-content">
	<?php if(isset($emailSent) && $emailSent == true) {  ?>
		<div class="thanks"> 			
				<style>
					.download{display:none}
					#contactForm{display:none}
				</style>
				<script>
				//browser form history remove every refresh
					if ( window.history.replaceState ) {
						window.history.replaceState( null, null, window.location.href );
					}
				</script>  
				<?php
					$urlname = esc_html($atts['urlname']); 
					 $title = esc_html($atts['title']);  
				?>  
				<?php if($donloadurl == $urlname ){ ?> 
					<script>	 
						var urlName = "<?php  esc_html_e($donloadurl); ?>";
						var urltitle = "<?php esc_html_e($atts['title']); ?>";
						//window.open(urlName, '_blank', 'new.pdf'); 						 
					</script>   
                   <script> 					
					jQuery(document).ready(function() {
						axios({
                              url:'<?php esc_html_e($donloadurl); ?>',
                              method:'GET',
                              responseType: 'blob'
						  })
						  .then((response) => {
							 const url = window.URL
							 .createObjectURL(new Blob([response.data],{
								 "type": "text/pdf;charset=utf8;"
							 }));
								const link = document.createElement('a');
								link.href = url;
								link.setAttribute('download', '<?php esc_html_e($atts['title']); ?>.pdf');
								document.body.appendChild(link);
								link.click();
						  })
					});
                   </script>			        
					<?php
					// Create post object to save data
					$my_post = array(
						'post_title'    => wp_strip_all_tags( $name ),
						'post_content'  => $body,
						'post_status'   => 'publish',
						'post_author'   => 1,
						'post_category' => array( 8,39 ),
						'post_type'		=> 'infomat'
					);
					// Insert the post into the database
					wp_insert_post( $my_post );
					?>
				<?php   }  ?>
		</div>
	<?php } else { ?>		
		<?php if(isset($hasError) || isset($captchaError)) { ?>
			<p class="error">Sorry, an error occured.<p>
		<?php } ?> 
		<!-- The Modal -->
		<div id="<?php esc_html_e($echos);?>" class="modal">
		<!-- Modal content -->
			<div class="modal-content">
				<div class="modal-header">
					<span class="close close<?php esc_html_e($echos);?>">&times;</span>
					<h4>Restricted download</h4>
				</div> 
				<div class="modal-body">		
				<!--form--> 
				<form action="" id="contactForm" method="post">					
							<ul class="contactform">			
								<b class="entry-title">To download this file, please fill in this form</b>	
							<li>
								<label for="email">Email</label>
								<input type="text" name="email" id="email" value="" class="required requiredField email" required/>				
								<?php if($emailError != '') { ?>
									<span class="error"><?php esc_html_e($emailError);?></span>
								<?php } ?>
							</li>				
							<li>
								<label for="contactName">Name</label>
								<input type="text" name="contactName" id="contactName" value="" class="required requiredField" required/>
								<?php if($nameError != '') { ?>
									<span class="error"><?php esc_html_e($nameError);?></span>
								<?php } ?>
							</li>
							<li>
								<label for="contactName"> Job title</label>
								<input type="text" name="jobtitle" id="contactName" value="" class="required requiredField" required/>
								<?php if($nameError != '') { ?>
									<span class="error"><?php esc_html_e($nameError);?></span>
								<?php } ?>
							</li>
							<input type="hidden" name="urlnameinput" value="<?php esc_html_e($atts['urlname']);?>" required/>
							<li><label for="commentsText">Company</label>
								<input type="text" name="comments" id="commentsText" class="required requiredField" value=""  required/>			
								<?php if($commentError != '') { ?>
									<span class="error"><?php esc_html_e($commentError);?></span>
								<?php } ?>
							</li>
							<li>			
								<input type="checkbox" id="vehicle1" class="checkboxes" name="vehicle1" value="" required>
								<p>Check In</p>
							</li>
							<li class="getyourfile">
								<input type="submit" value="GET YOUR FILE"></input>
							</li>
						</ul> 		
						<input type="hidden" name="submitted" id="submitted" value="true" />
					</form>
				<!--end form-->   
				</div>    
			<div class="modal-footer"></div>
		</div>
	<?php } ?>
</div><!-- .entry-content --> 
<script> 
// Get the modal 
var <?php esc_html_e($echos);?> = document.getElementById("<?php esc_html_e($echos);?>"); 
// Get the button that opens the modal
var btn = document.getElementById("<?php esc_html_e($echos);?>1");
// Get the <span> element that closes the modal
var span<?php esc_html_e($echos);?> = document.getElementsByClassName("close<?php esc_html_e($echos);?>")[0];
// When the user clicks the button, open the modal 
btn.onclick = function() {
	<?php esc_html_e($echos);?>.style.display = "block";
}
// When the user clicks on <span> (x), close the modal
span<?php esc_html_e($echos);?>.onclick = function() {
	<?php esc_html_e($echos);?>.style.display = "none";
}
// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == <?php esc_html_e($echos);?>) {
    <?php esc_html_e($echos);?>.style.display = "none";
  }
} 
</script>
<?php   
 return ob_get_clean();
}
add_shortcode('formtodownload','dpbsf_shortcode_wrapper');
//<-- custom post --> 
add_action( 'init', 'dpbsf_download_posttype' );
function dpbsf_download_posttype() {
 $labels = array(
  'name'               => _x( 'Infomats', 'post type general name', 'dpbsf' ),
  'singular_name'      => _x( 'Infomat', 'post type singular name', 'dpbsf' ),
  'menu_name'          => _x( 'Infomats', 'admin menu', 'dpbsf' ),
  'name_admin_bar'     => _x( 'Infomat', 'add new on admin bar', 'dpbsf' ),
  'add_new'            => _x( 'Add New', 'Infomat', 'dpbsf' ),
  'add_new_item'       => __( 'Add New Infomat', 'dpbsf' ),
  'new_item'           => __( 'New Infomat', 'dpbsf' ),
  'edit_item'          => __( 'Edit Infomat', 'dpbsf' ),
  'view_item'          => __( 'View Infomat', 'dpbsf' ),
  'all_items'          => __( 'All Infomats', 'dpbsf' ),
  'search_items'       => __( 'Search Infomats', 'dpbsf' ),
  'parent_item_colon'  => __( 'Parent Infomats:', 'dpbsf' ),
  'not_found'          => __( 'No Infomats found.', 'dpbsf' ),
  'not_found_in_trash' => __( 'No Infomats found in Trash.', 'dpbsf' )
 );
 $args = array(
  'labels'             => $labels,
  'description'        => __( 'Description.', 'dpbsf' ),
  'public'             => true,
  'publicly_queryable' => true,
  'show_ui'            => true,
  'show_in_menu'       => true,
  'query_var'          => true,
  'rewrite'            => array( 'slug' => 'infomat' ),
  'capability_type'    => 'post',
  'has_archive'        => true,
  'hierarchical'       => false,
  'menu_position'      => null,
  'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
 );
 register_post_type( 'infomat', $args );
} 

// Dashboard Front Show settings page
register_activation_hook(__FILE__, 'dpbsf_plugin_activate');
add_action('admin_init', 'dpbsf_plugin_redirect');
function dpbsf_plugin_activate() {
    add_option('dpbsf_plugin_do_activation_redirect', true);
}
function dpbsf_plugin_redirect() {
    if (get_option('dpbsf_plugin_do_activation_redirect', false)) {
        delete_option('dpbsf_plugin_do_activation_redirect');
        if(!isset($_GET['activate-multi']))
        {
            wp_redirect("edit.php?post_type=infomat&page=settings");
        }
    }
}
//side setting link
add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), 'dpbsf_plugin_action_links' );
function dpbsf_plugin_action_links( $actions ) {
   $actions[] = '<a href="'. esc_url( get_admin_url(null, 'edit.php?post_type=infomat&page=settings') ) .'">Settings</a>';
   $actions[] = '<a href="https://m.me/md.shahinur.islam.96" target="_blank">Support for contact</a>';
   return $actions;
}
add_action('admin_menu', 'dpbsf_register_my_custom_submenu_page'); 
function dpbsf_register_my_custom_submenu_page() {
    add_submenu_page(
        'edit.php?post_type=infomat',
        'Settings',
        'Settings',
        'manage_options',
        'settings',
        'dpbsf_my_custom_submenu_page_callback' );
} 
function dpbsf_my_custom_submenu_page_callback() {
    ?>
<h1>
<?php esc_html_e( 'Welcome to Download PDF After Submit Form.', 'dpbsf' ); ?>
</h1>
<h3><?php esc_html_e( 'Copy and paste this shortcode here:', 'dpbsf' );?></h3>
<p><?php esc_html_e( '[formtodownload urlname="url" title="1"]', 'dpbsf' );?></p> 
<?php
}
